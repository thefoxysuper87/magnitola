RashkinskRadios = RashkinskRadios or {}

surface.CreateFont( "Magnitola1", {
	   font = "Roboto", -- Use the font-name which is shown to you by your operating system Font Viewer, not the file name
	   extended = false,
	   size = 15,
	   weight = 500,
	   blursize = 0,
	   scanlines = 0,
	   antialias = true,
	   underline = false,
	   italic = false,
	   strikeout = false,
	   symbol = false,
	   rotary = false,
	   shadow = false,
	   additive = false,
	   outline = false,
	   extended = true
} )

surface.CreateFont( "Magnitola2", {
	   font = "Roboto", -- Use the font-name which is shown to you by your operating system Font Viewer, not the file name
	   extended = false,
	   size = 20,
	   weight = 500,
	   blursize = 0,
	   scanlines = 0,
	   antialias = true,
	   underline = false,
	   italic = false,
	   strikeout = false,
	   symbol = false,
	   rotary = false,
	   shadow = false,
	   additive = false,
	   outline = false,
	   extended = true
} )

	
--PlayerRashkinskPlaylists = {["Default Playlist"] = defolttable,}

local UseFake3D = false


local function getcar(car)
	if simfphys and IsValid(car) and IsValid(car:GetParent()) and simfphys.IsCar and isfunction(simfphys.IsCar) and simfphys.IsCar(car:GetParent()) then
		return car:GetParent()
	end
	return car
end

local function calcVolume(ply, car, volume)
	local plyVeh = ply:GetVehicle()

	if IsValid(plyVeh) and IsValid(plyVeh:GetParent()) and plyVeh:GetParent():IsVehicle() then
		plyVeh = plyVeh:GetParent()
	end
	if IsValid(plyVeh) and plyVeh == car then
		return volume
	end

	return volume * MagnitolaConfig.OutOfCarModifier
end

net.Receive("RashMagnitola_BroadcastShit", function()
	local str = net.ReadString()
	local car = net.ReadEntity()
	local uid = net.ReadString()
	if IsValid(car) and car:IsVehicle() then
		local volume = 1
		if IsValid(RashkinskRadios[car:EntIndex()]) then
			if car.MagnitolaVolume != nil then
				volume = car.MagnitolaVolume
			else
				volume = RashkinskRadios[car:EntIndex()]:GetVolume()
			end
			RashkinskRadios[car:EntIndex()]:Stop()
			RashkinskRadios[car:EntIndex()] = nil
		end
		local tags = "3d noblock"
		if UseFake3D then
			tags = "noblock"
		end
		sound.PlayURL(str, tags, function(station)
			if IsValid(station) then
				if RashkinskRadios[car:EntIndex()] and IsValid(RashkinskRadios[car:EntIndex()]) then
					if car.MagnitolaVolume != nil then
						volume = car.MagnitolaVolume
					else
						volume = RashkinskRadios[car:EntIndex()]:GetVolume()
					end
					RashkinskRadios[car:EntIndex()]:Stop()
					RashkinskRadios[car:EntIndex()] = nil
				end
				station:SetPos(car:GetPos() + Vector(0, 0, 50))
				station:Set3DFadeDistance( 300, 0 )
				station:Play()
				car.MagnitolaVolume = volume
				station:SetVolume(calcVolume(LocalPlayer(), car, volume))
				RashkinskRadios[car:EntIndex()] = station
				if station:GetLength() < 0 and !car.CurRadio then
					car.CurRadio = tostring(87.5 + tonumber(util.CRC(str)) % 20.5)
				end
			else
				print("INVALID URL")
			end
		end)
		car.CurSongTeim = nil
		car.ChangeTime = nil
		car.SendedShit = nil
		if car.CurrentPlaylistData and uid != LocalPlayer():UniqueID() then
			car.CurrentPlaylistData = nil
		end
	end
end)

net.Receive("RashMagnitola_BroadcastData", function()
	local car = net.ReadEntity()
	local whattodo = net.ReadString()
	if IsValid(car) and IsValid(RashkinskRadios[car:EntIndex()]) then
		if whattodo == "Volume" then
			local volume = net.ReadFloat()
			car.MagnitolaVolume = volume
			RashkinskRadios[car:EntIndex()]:SetVolume(calcVolume(LocalPlayer(), car, volume))
		elseif whattodo == "Time" then
			local time = net.ReadInt(32)
			local radio = RashkinskRadios[car:EntIndex()]
			if radio:GetLength() >= 0 then
				radio:SetTime(time)
				radio:Play()
			end
			
		end
		car.CurSongTeim = nil
		car.ChangeTime = nil
	end
end)
	
surface.CreateFont("MagnitolaDisplayFont", {
	font = "Time",
	extended = false,
	size = 35,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
})

surface.CreateFont("MagnitolaDisplaySmallFont", {
	font = "Arial",
	extended = false,
	size = 12,
	weight = 500,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
})

surface.CreateFont("MagnitolaRadioButton", {
	font = "Arial",
	extended = false,
	size = 20,
	weight = 2000,
	blursize = 1,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = true,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
})
	
hook.Add("EntityEmitSound", "SetVolumeToLowerMagnitola", function(data)
	local ent = data.Entity
	if IsValid(ent) and ent:IsVehicle() and IsValid(RashkinskRadios[ent:EntIndex()]) then
		data.Volume = 0.3
		return true
	end
end)
	
local clicksound = Sound("rashkinsk/magnitola/button_click.wav")
local backgroundmat = Material("rashkinsk/magnitola/background.png")
local discmat = Material("rashkinsk/magnitola/icon_disc.png")

local material = Material("rashkinsk/magnitola/button_volume.png")
local backgroundmatsound = Material("rashkinsk/magnitola/button_volume_background.png")
local backgroundmatsoundact = Material("rashkinsk/magnitola/button_volume_background_active.png")

local trackmat = Material("rashkinsk/magnitola/button_2.png")
local trackmatact = Material("rashkinsk/magnitola/button_2_active.png")

local radiomat = Material("rashkinsk/magnitola/button_1.png")
local radiomatact = Material("rashkinsk/magnitola/button_1_active.png")

local selmat = Material("rashkinsk/magnitola/button_select.png")
local selmatact = Material("rashkinsk/magnitola/button_select_active.png")
	
function RashkinskCloseMagnitola()
	if RashkinskMagnitolaFrame and ValidPanel(RashkinskMagnitolaFrame) then
		if ValidPanel(RashkinskMagnitolaFrame.PlaylistMenu) then
			RashkinskMagnitolaFrame.PlaylistMenu:Close()
		end
		RashkinskMagnitolaFrame:Close()
		RashkinskMagnitolaFrame = nil
	end
end

local function SendLinkCar(str, car)
	if IsValid(LocalPlayer()) and LocalPlayer() != NULL and IsValid(car) and car:IsVehicle() then
		net.Start("RashMagnitola_GetLink")
			net.WriteString(str)
			net.WriteEntity(car)
		net.SendToServer()
		car.CurRadio = nil
		car.RadioPreset = nil
	end
end
	
local function SendLink(str)
	if IsValid(LocalPlayer()) and LocalPlayer() != NULL and IsValid(LocalPlayer():GetVehicle()) then
		/*net.Start("RashMagnitola_GetLink")
			net.WriteString(str)
			net.WriteEntity(LocalPlayer():GetVehicle())
		net.SendToServer()
		LocalPlayer():GetVehicle().CurRadio = nil
		LocalPlayer():GetVehicle().RadioPreset = nil*/
		SendLinkCar(str, getcar(LocalPlayer():GetVehicle()))
	end
end

local function SavePlaylist()
	if PlayerRashkinskPlaylists then
		local rawdata = util.TableToJSON(PlayerRashkinskPlaylists)
		file.Write("RashkinskMagnitolaPlaylists.txt", rawdata)
	end
end

local function CreateDefaultPlaylist()
	local tabl = {["Default Playlist"] = table.Copy(MagnitolaConfig.DefaultPlaylist)}
	PlayerRashkinskPlaylists = tabl
	SavePlaylist()
end

local function ValidatePlaylist()
	if !PlayerRashkinskPlaylists then
		if file.Exists("RashkinskMagnitolaPlaylists.txt", "DATA") then
			local rawdata = file.Read("RashkinskMagnitolaPlaylists.txt", "DATA")
			local finaltable = util.JSONToTable(rawdata)
			if finaltable and type(finaltable) == "table" then
				PlayerRashkinskPlaylists = finaltable
			else
				CreateDefaultPlaylist()
			end
		else
			CreateDefaultPlaylist()
		end
	end
end

local function StartPlaylist(playlist, track)
	if IsValid(LocalPlayer()) and LocalPlayer() != NULL and IsValid(LocalPlayer():GetVehicle()) then
		local car = getcar(LocalPlayer():GetVehicle())
		ValidatePlaylist()
		local data = PlayerRashkinskPlaylists[playlist]
		if data then
			local newdata = data[track]
			if newdata then
				local link = newdata.link or ""
				SendLinkCar(link, car)
				car.CurrentPlaylistData = {playlist = playlist, track = track}
			end
		end
	end
end

local function PlaylistNextTrack(car)
	if IsValid(car) and car:IsVehicle() then
		local cardata = car.CurrentPlaylistData
		if cardata then
			local data = PlayerRashkinskPlaylists[cardata.playlist]
			local track = cardata.track + 1
			if track > #data then
				track = 1
			end
			local newdata = data[track]
			SendLinkCar(newdata.link, car)
			car.CurrentPlaylistData = {playlist = cardata.playlist, track = track}
		end
	end
end

local function PlaylistPrevTrack(car)
	if IsValid(car) and car:IsVehicle() then
		
		local cardata = car.CurrentPlaylistData
		if cardata then
			local data = PlayerRashkinskPlaylists[cardata.playlist]
			local track = cardata.track - 1
			if track < 1 then
				track = #data
			end
			if IsValid(RashkinskRadios[car:EntIndex()]) then
				local radio = RashkinskRadios[car:EntIndex()]
				if radio:GetLength() > 0 and radio:GetTime() > 4 then
					track = cardata.track
				end
			end
			local newdata = data[track]
			SendLinkCar(newdata.link, car)
			car.CurrentPlaylistData = {playlist = cardata.playlist, track = track}
		end
	end
end

local crossmat = Material("icon16/cancel.png", "noclamp")
local cassetemat = Material("Rashkinsk/Magnitola/cassette_new.png", "noclamp")
local cassetetop = Material("rashkinsk/magnitola/cassette_top.png", "noclamp")
local playmat = Material("icon16/control_play_blue.png", "noclamp")
local addmat = Material("icon16/add.png", "noclamp")
local boxmat = Material("rashkinsk/magnitola/box.png", "noclamp")

local function OpenPlaylistEditMenu()
	local PlaylistMenu = vgui.Create("DFrame")
	PlaylistMenu:SetSize(500, 600)
	PlaylistMenu:SetPos(ScrW() / 2 - PlaylistMenu:GetWide() / 2, ScrH())
	PlaylistMenu:SetTitle("")
	PlaylistMenu:DockPadding(20, 40, 20, 40)
	PlaylistMenu:NoClipping(true)
	function PlaylistMenu:Paint(w,h)
		surface.SetDrawColor(color_white)
		surface.SetMaterial(boxmat)
		surface.DrawTexturedRect(0, 0, w, h)
	end

	PlaylistMenu:MoveTo(ScrW() / 2 - PlaylistMenu:GetWide() / 2, ScrH() / 2 - PlaylistMenu:GetTall() / 2, 0.5, 0, -1)

	//close rewriting

	local closeButton

	PlaylistMenu.oldClose = PlaylistMenu.Close

	function PlaylistMenu:Close()
		self:MoveTo(ScrW() / 2 - PlaylistMenu:GetWide() / 2, ScrH(), 0.5, 0, -1, function() self:oldClose() end)
		if ValidPanel(closeButton) then
			closeButton:CloseAnim()
		end
	end

	local casseteList = vgui.Create( "DPanelList", PlaylistMenu )
	casseteList:Dock( FILL )
	casseteList:EnableVerticalScrollbar(true)
	casseteList:SetSpacing( 5 )
	function casseteList:Paint(w,h)
	end

	local function UpdatePlaylists()
		casseteList:Clear()
		ValidatePlaylist()
		local tapeHeight = 300
		local sizeHeight = 50
		for k,v in pairs(PlayerRashkinskPlaylists) do
			local pan = vgui.Create("DPanel", casseteList)
			pan:DockPadding(16, 8, 15, 13)
			local function isSelected()
				return PlaylistMenu.ChoosenTape == k
			end
			pan:SetTall(isSelected() and tapeHeight or sizeHeight)
			pan.selectLerp = isSelected() and 1 or 0
			function pan:Paint(w,h)
				if isSelected() then
					self.selectLerp = Lerp(FrameTime() * 10, self.selectLerp, 1)
					if self.selectLerp >= 0.9 and (!pan.Header:IsVisible() or !pan.List:IsVisible()) then
						pan.Header:SetVisible(true)
						pan.List:SetVisible(true)
					end
				else
					self.selectLerp = Lerp(FrameTime() * 10, self.selectLerp, 0)
					if self.selectLerp <= 0.1 and !pan.Header:IsVisible() then
						pan.Header:SetVisible(true)
					end
				end
				if self.selectLerp >= 0.01 and self.selectLerp <= 0.99 then
					casseteList:PerformLayout()
				end
				h = Lerp(self.selectLerp, sizeHeight, tapeHeight)
				self:SetTall(h)
				surface.SetDrawColor(255,255,255,255)
				//tape top
				surface.SetMaterial(cassetetop)
				local topHeight = sizeHeight * (1 - self.selectLerp)
				surface.DrawTexturedRect(0, 0, w, topHeight)
				//fulltape
				surface.SetMaterial(cassetemat)
				surface.DrawTexturedRect(0, topHeight, w, h - topHeight)
			end
			pan.Header = vgui.Create("DPanel", pan)
			pan.Header:Dock(TOP)
			pan.Header:SetTall(35)
			pan.Header:DockPadding(0, 0, 0, 5)
			pan.Header:DockMargin(0, 0, 0, 5)
			function pan.Header:Paint(w, h)
				draw.SimpleText(k, "DermaLarge", 10, h / 2, Color(0,0,0,255), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			end

			pan.Header.RemoveButton = vgui.Create("DButton", pan.Header)
			pan.Header.RemoveButton:Dock(RIGHT)
			pan.Header.RemoveButton:SetWide(35)
			pan.Header.RemoveButton:SetText("")
			function pan.Header.RemoveButton:Paint(w,h)
				local imageW = 25
				surface.SetDrawColor(255,255,255,255)
				surface.SetMaterial(crossmat)
				surface.DrawTexturedRect(w / 2 - imageW / 2, h / 2 - imageW / 2, imageW, imageW)
			end
			function pan.Header.RemoveButton:DoClick()
				PlayerRashkinskPlaylists[k] = nil
				if isSelected() then
					PlaylistMenu.ChoosenTape = nil
				end
				SavePlaylist()
				UpdatePlaylists()
			end

			pan.Header.PlayButton = vgui.Create("DButton", pan.Header)
			pan.Header.PlayButton:SetText("")
			pan.Header.PlayButton:SetWide(35)
			pan.Header.PlayButton:Dock(RIGHT)
			function pan.Header.PlayButton:Paint(w,h)
				local imageW = 25
				surface.SetDrawColor(255,255,255,255)
				surface.SetMaterial(playmat)
				surface.DrawTexturedRect(w / 2 - imageW / 2, h / 2 - imageW / 2, imageW, imageW)
			end
			function pan.Header.PlayButton:DoClick()
				if #v > 0 then
					StartPlaylist(k,1)
					PlaylistMenu:Close()
				end
			end

			pan.Header.SelectButton = vgui.Create("DButton", pan.Header)
			pan.Header.SelectButton:Dock(FILL)
			pan.Header.SelectButton:SetText("")
			function pan.Header.SelectButton:Paint(w, h)
			end
			function pan.Header.SelectButton:DoClick()
				if isSelected() then
					PlaylistMenu.ChoosenTape = nil
				else
					PlaylistMenu.ChoosenTape = k
				end
				pan.Header:SetVisible(false)
				pan.List:SetVisible(false)
			end

			pan.List = vgui.Create("DPanelList", pan)
			pan.List:Dock(FILL)
			pan.List:EnableVerticalScrollbar(true)

			for k2,v2 in pairs(v) do
				local panel = vgui.Create("DPanel", pan.List)
				panel:SetSize(pan.List:GetWide(), 30)
				function panel:Paint(w,h)
					surface.SetDrawColor(0,0,0,255)
					surface.DrawRect(0,h-1,w,1)
					draw.SimpleText("[" .. k2 .. "]  " .. v2.link, "Default", 2, h-3, Color(0,0,0,255), TEXT_ALIGN_LEFT, TEXT_ALIGN_BOTTOM)
				end
				panel.RemoveBut = vgui.Create("DButton", panel)
				panel.RemoveBut:SetSize(15,15)
				panel.RemoveBut:SetText("")
				function panel.RemoveBut:Paint(w,h)
					surface.SetDrawColor(255,255,255,255)
					surface.SetMaterial(crossmat)
					surface.DrawTexturedRect(0,0,w,h)
				end
				function panel.RemoveBut:DoClick()
					v[k2] = nil
					local x = 1
					for k3,v3 in pairs(v) do
						v[k3] = nil
						v[x] = v3
						x = x + 1
					end
					SavePlaylist()
					UpdatePlaylists()
				end
				pan.List:AddItem(panel)
				
				function panel.RemoveBut:Think()
					self:SetPos(panel:GetWide()-20, 10)
				end
			end
			local addnewpanelshit = vgui.Create("DButton", pan.List)
			addnewpanelshit:SetText("Add new track")
			addnewpanelshit:SetTall(30)
			function addnewpanelshit:Paint(w,h)
				surface.SetDrawColor(0,0,0,255)
				surface.DrawRect(0,h-1, w, 1)
				surface.SetDrawColor(255,255,255,255)
				surface.SetMaterial(addmat)
				surface.DrawTexturedRect(0,0,h,h)
			end
			function addnewpanelshit:DoClick()
				Derma_StringRequest("Link", "Insert link", "", function(str)
					if str != "" then
						table.insert(v, {link = str, IsYoutube = false})
						SavePlaylist()
						UpdatePlaylists()
					end
				end)
			end
			
			pan.List:AddItem(addnewpanelshit)

			pan.List:SetVisible(PlaylistMenu.ChoosenTape == k)

			casseteList:AddItem(pan)
		end
		local button = vgui.Create("DButton", casseteList)
		button:SetText("")
		button:SetTall(sizeHeight)
		function button:DoClick()
			Derma_StringRequest("Name", "Insert Name", "", function(str)
				if str != "" then
					PlayerRashkinskPlaylists[str] = {}
					SavePlaylist()
					UpdatePlaylists()
				end
			end)
		end
		function button:Paint(w, h)
			surface.SetDrawColor(255,255,255,255)
			surface.SetMaterial(cassetetop)
			surface.DrawTexturedRect(0, 0, w, h)
			draw.SimpleText("New cassete", "DermaLarge", 65, h / 2, Color(0, 0, 0, 255), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			surface.SetMaterial(addmat)
			surface.DrawTexturedRect(15, h / 2 - 15, 30, 30)
		end
		casseteList:AddItem(button)
	end
	UpdatePlaylists()

	closeButton = vgui.Create("DButton", PlaylistMenu)
	closeButton:SetSize(PlaylistMenu:GetWide(), PlaylistMenu:GetTall())
	closeButton:SetEnabled(true)

	function closeButton:Paint(w, h)
		surface.DisableClipping(true)
		surface.SetDrawColor(color_white)
		surface.SetMaterial(boxmat)
		surface.DrawTexturedRectUV(0, 0, w, h, 0.15, 0.15, 0.85, 0.85)
		surface.DisableClipping(false)
	end

	closeButton:MoveTo(PlaylistMenu:GetWide() - 1, 0, 0.5, 0, -1, function()
		closeButton:SetEnabled(true)
	end)

	closeButton:SizeTo(0, PlaylistMenu:GetTall(), 0.5, 0, -1, function()
		closeButton:SizeTo(PlaylistMenu:GetWide(), PlaylistMenu:GetTall(), 0.5, 0, -1)
	end)

	function closeButton:CloseAnim()
		closeButton:SizeTo(0, PlaylistMenu:GetTall(), 0.25, 0, -1, function()
			closeButton:MoveTo(0, 0, 0.25, 0, -1)
			closeButton:SizeTo(PlaylistMenu:GetWide(), PlaylistMenu:GetTall(), 0.25, 0, -1)
		end)
	end

	function closeButton:DoClick()
		PlaylistMenu:Close()
	end
	return PlaylistMenu
end



hook.Add("Tick", "ThinkRashMagnitolas", function()
	for k,v in pairs(RashkinskRadios) do
		if IsValid(Entity(k)) and Entity(k):IsVehicle() then
			local car = Entity(k)
			local pos = car:GetPos() + Vector(0,0,50)
			local att = car:LookupAttachment("vehicle_driver_eyes")
			if att then
				local posang = car:GetAttachment(att)
				if posang then
					pos = posang.Pos + posang.Ang:Forward() * 30
				end
			end
			v:SetPos(pos)

			if UseFake3D then
				local dist = LocalPlayer():GetPos():Distance(pos)-200
				local vol = 1-dist/600
				v:SetVolume(calcVolume(LocalPlayer(), car, math.Clamp(vol, 0, 1) * car.MagnitolaVolume))
			else
				v:SetVolume(calcVolume(LocalPlayer(), car, car.MagnitolaVolume))
			end
			if car.CurrentPlaylistData then
				if v:GetLength() > 0 and v:GetTime() >= v:GetLength() and !car.SendedShit then
					/*local cardata = car.CurrentPlaylistData
					local data = PlayerRashkinskPlaylist[cardata.playlist]
					local track = cardata.track + 1
					if track > #data then
						track = 1
					end
					local newdata = data[track]
					SendLinkCar()
					car.CurrentPlaylistData = {playlist = playlist, track = track}*/
					PlaylistNextTrack(car)
					car.SendedShit = true
				end
			end
		else
			v:Stop()
			RashkinskRadios[k] = nil
		end
	end
end)

local function ChangeVolume(vol)
	if IsValid(LocalPlayer()) and LocalPlayer() != NULL and IsValid(LocalPlayer():GetVehicle()) then
		net.Start("RashMagnitola_ChangeData")
			net.WriteString("Volume")
			//net.WriteEntity(LocalPlayer():GetVehicle())
			net.WriteFloat(vol)
		net.SendToServer()
	end
end

	
local function ChangeTime(time)
	net.Start("RashMagnitola_ChangeData")
		net.WriteString("Time")
		net.WriteInt(time, 32)
	net.SendToServer()
		
end
	
function RashkinskOpenMagnitola()
		
		

	local dframe = vgui.Create("DFrame")
	local frameheight, framewidth = 170, 700
	local gap = 5
	dframe:SetSize(framewidth, frameheight)
	dframe:SetPos(ScrW()/2-framewidth/2, ScrH())
	dframe:MoveTo(ScrW()/2-framewidth/2, ScrH()-frameheight-gap, 0.2, 0, -1)
	dframe:SetTitle("")
	dframe.Opened = true
	dframe:SetDraggable(false)
	dframe:ShowCloseButton(false)
	RashkinskMagnitolaFrame = dframe


	function dframe:Paint(w,h)
		local car = getcar(LocalPlayer():GetVehicle())
		surface.SetDrawColor(color_white)
		surface.SetMaterial(backgroundmat)
		surface.DrawTexturedRect(0,0,w,h)
		surface.SetMaterial(discmat)
		surface.DrawTexturedRect(320,90,80,30)
		local x1, y1 = self:LocalToScreen(110, 0)
		local x2, y2 = self:LocalToScreen(320, 700)
		
		draw.RoundedBox(3, 115, 92, 26, 9, Color(66,190,124))
		draw.SimpleText("AUTO", "MagnitolaDisplaySmallFont", 115, 92+5, Color(0,0,0), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			
		draw.RoundedBox(3, 115, 108, 26, 9, Color(66,190,124))
		draw.SimpleText("LOUD", "MagnitolaDisplaySmallFont", 115, 108+5, Color(0,0,0), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
		
		local text = "KENWOOD"
		local text2
		if IsValid(car) and IsValid(RashkinskRadios[car:EntIndex()]) then
			local station = RashkinskRadios[car:EntIndex()]
			if station:GetLength() < 0 and car.CurRadio then
				text = car.CurRadio
				text2 = "FM"
			elseif station:GetTime() and station:GetLength() > 0 then
				if car.ChangeTime then
					if !car.CurSongTeim then
						car.CurSongTeim = station:GetTime()
					end
					local delto = 0
					if car.ChangeTime >= 0 then
						delto = (CurTime() - car.ChangeTime) * 6
					else
						delto = -(CurTime() + car.ChangeTime) * 6
					end
					//delto = math.Clamp(delto, 0, station:GetLength())
					local teim = math.Clamp(car.CurSongTeim + delto, 0, station:GetLength())
					text = string.ToMinutesSeconds(teim)
				else
					text = string.ToMinutesSeconds(station:GetTime())
				end
				text2 = "TAPE"
				draw.SimpleText("DISC", "MagnitolaDisplaySmallFont", 320, 95, Color(255,255,198), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			end
		end
		//render.SetScissorRect(x1, y1, x2, y2, true)
			
		draw.SimpleText(text, "MagnitolaDisplayFont", 110+120, 105, Color(255,255,198), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			
		//render.SetScissorRect(0,0,0,0,false)
		if text2 then
			draw.SimpleText(text2, "MagnitolaDisplayFont", 320+80, 105, Color(255,255,198), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
		end
		if IsValid(car) then
			if car.RadioPreset then
				draw.SimpleText(car.RadioPreset, "MagnitolaDisplayFont", 480, 105, Color(66,190,124), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
				draw.SimpleText("ST", "MagnitolaDisplaySmallFont", 500, 95, Color(66,190,124), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			end
		end
	end
		
	local button = vgui.Create("DButton", dframe)
	button:SetPos(11, 62)
	button:SetSize(70, 70)
	button:SetText("")
	button.Ang = -40
	button.Dif = 1
	RashkinskMagnitolaFrame.VolumeButton = button
	local halfsize = button:GetWide()/2
	timer.Simple(0.1, function()
		local car = getcar(LocalPlayer():GetVehicle())
		if IsValid(car) and IsValid(RashkinskRadios[car:EntIndex()]) then
			local station = RashkinskRadios[car:EntIndex()]
			if button.Dif != car.MagnitolaVolume then
				button.Dif = car.MagnitolaVolume
				button.Ang = math.NormalizeAngle(-40+260*(1-button.Dif))
			end
		end
	end)
	button.Sended = true
	function button:Think()
		if self.Depressed then
			self.Sended = false
			local x, y = self:CursorPos()
       
			x = x-halfsize
			y = y-halfsize
			local ang = -math.deg(math.atan2(y, x))
			if ang > -140 and ang < -90 then
				self.Ang = -140
			elseif ang < -40 and ang >= -90 then
				self.Ang = -40
			else
				self.Ang = ang
			end
			local dif = math.AngleDifference(-140, self.Ang)
			if dif < 0 then
				dif = (180+(180+dif))
			end
			dif = dif/260
			self.Dif = dif
		elseif !self.Sended then
			self.Sended = true
			if self.Dif then
				ChangeVolume(self.Dif)
			end
		end
	end

	function button:OnCursorEntered()
		button.CursorIn = true
	end
		
	function button:OnCursorExited()
		button.CursorIn = false
	end

	function button:Paint(w,h)
		surface.SetDrawColor(color_white)
		if button.CursorIn or button.Depressed then
			surface.SetMaterial(backgroundmatsoundact)
		else
			surface.SetMaterial(backgroundmatsound)
		end
		surface.DrawTexturedRect(0,0,w,h)
		surface.SetMaterial(material)
		surface.DrawTexturedRectRotated(halfsize, halfsize, w, h, self.Ang)
	end

	local closebutton = vgui.Create("DButton", dframe)
	closebutton:SetSize(100, 60)
	closebutton:SetPos(50, 0)
	closebutton:SetText("")
	RashkinskMagnitolaFrame.CloseButton = closebutton
	function closebutton:DoClick()
		if dframe.Opened then
			dframe:MoveTo(ScrW()/2-framewidth/2, ScrH()-20, 0.2, 0, -1)
		else
			dframe:MoveTo(ScrW()/2-framewidth/2, ScrH()-frameheight-gap, 0.2, 0, -1)
		end
		dframe.Opened = !dframe.Opened
		surface.PlaySound(clicksound)
	end
	local closemat = Material("rashkinsk/magnitola/button_power.png")
	function closebutton:Paint(w,h)
		surface.SetDrawColor(255,255,255,255)
		if dframe.Opened then
			surface.SetMaterial(closemat)
			surface.DrawTexturedRect(0,0,w,h)
		end
	end

	local Buttons = {}
	
	local function CreateButton(id, x, y, w, h, mat, matact, mirrorx, doclickfunc, additionalpaint, playsound)
		if playsound == nil then
			playsound = true
		end
		
		local betton = vgui.Create("DButton", dframe)
		betton:SetPos(x, y)
		betton:SetSize(w,h)
		betton:SetText("")

		function betton:OnCursorEntered()
			self.CursorIn = true
		end

		function betton:OnCursorExited()
			self.CursorIn = false
		end

		function betton:Paint(w,h)
			local neww, newh, newx, newy = w, h, 0, 0
			if self.ScaleBack and self.ScaleBack >= CurTime() then
				neww, newh, newx, newy = w-2, h-2, 1, 1
			end
			surface.SetDrawColor(color_white)
			if matact and mat then
				if self.CursorIn or self.Depressed then
					surface.SetMaterial(matact)
				else
					surface.SetMaterial(mat)
				end
				if mirrorx then
					surface.DrawTexturedRectUV(newx,newy,neww,newh, 1, 0, 0, 1)
				else
					surface.DrawTexturedRect(newx,newy,neww,newh)
				end
			elseif mat then
				surface.SetMaterial(mat)
				if mirrorx then
					surface.DrawTexturedRectUV(newx,newy,neww,newh, 1, 0, 0, 1)
				else
					surface.DrawTexturedRect(newx,newy,neww,newh)
				end
			end
				
			if additionalpaint then
				additionalpaint(self, w, h)
			end
		end


			
		function betton:DoClick()
			self.ScaleBack = CurTime() + 0.1
			if playsound then
				surface.PlaySound(clicksound)
			end
			doclickfunc(self)
		end
		Buttons[id] = betton
	end



	CreateButton("prevtrack", 575, 110, 55, 20, trackmat, trackmatact, false, function() end, nil, false)
		
	local prevtrack = Buttons["prevtrack"]
		
	function prevtrack:Think()
		local car = getcar(LocalPlayer():GetVehicle())
		if self.Depressed then
			if !self.StartedPress then
				self.StartedPress = CurTime()
				surface.PlaySound(clicksound)
			end
			self.ScaleBack = CurTime() + 0.1
			if IsValid(car) and IsValid(RashkinskRadios[car:EntIndex()]) and RashkinskRadios[car:EntIndex()]:GetLength() > 0 and (CurTime() - self.StartedPress) >= 0.5 and !car.ChangeTime then
				car.ChangeTime = -CurTime()
				car.StartTeim = RashkinskRadios[car:EntIndex()]:GetTime()
			end
		elseif self.StartedPress then
			if IsValid(car) and IsValid(RashkinskRadios[car:EntIndex()]) and RashkinskRadios[car:EntIndex()]:GetLength() > 0 then
				if (CurTime() - self.StartedPress) >= 0.5 then
					//changetime
					local time = (CurTime() - self.StartedPress - 0.5) * 6
					local curtim = car.StartTeim//RashkinskRadios[car:EntIndex()]:GetTime()
					local res = math.Max(curtim - time, 0)
					
					ChangeTime(res)
					car.StartTeim = nil
				else
					//prevtrack
					if IsValid(car) then
						PlaylistPrevTrack(car)
					end
				end
			end
			if CurTime() - self.StartedPress >= 0.1 then
				surface.PlaySound(clicksound)
			end
			self.StartedPress = nil
		end
	end

	CreateButton("nexttrack", 640, 110, 55, 20, trackmat, trackmatact, true, function() end)
		
	local nexttrack = Buttons["nexttrack"]
		
	function nexttrack:Think()
		local car = getcar(LocalPlayer():GetVehicle())
		if self.Depressed then
			if !self.StartedPress then
				self.StartedPress = CurTime()
				surface.PlaySound(clicksound)
			end
			self.ScaleBack = CurTime() + 0.1
			if IsValid(car) and IsValid(RashkinskRadios[car:EntIndex()]) and RashkinskRadios[car:EntIndex()]:GetLength() > 0 and (CurTime() - self.StartedPress) >= 0.5 and !car.ChangeTime then
				car.ChangeTime = CurTime()
				car.StartTeim = RashkinskRadios[car:EntIndex()]:GetTime()
			end
		elseif self.StartedPress then
			if IsValid(car) and IsValid(RashkinskRadios[car:EntIndex()]) and RashkinskRadios[car:EntIndex()]:GetLength() > 0 then
				if (CurTime() - self.StartedPress) >= 0.5 then
					//changetime
					local time = (CurTime() - self.StartedPress - 0.5) * 6
					local curtim = car.StartTeim//RashkinskRadios[car:EntIndex()]:GetTime()
					local res = math.Min(curtim + time, RashkinskRadios[car:EntIndex()]:GetLength())

					ChangeTime(res)
					car.StartTeim = nil
				else
					//nexttrack
					if IsValid(car) then
						PlaylistNextTrack(car)
					end
				end
			end
			if CurTime() - self.StartedPress >= 0.1 then
				surface.PlaySound(clicksound)
			end
			self.StartedPress = nil
		end
	end

	CreateButton("ChoseTrackGay", 245, 12, 38, 48, selmat, selmatact, false, function() 
		/*Derma_StringRequest("URL", "Insert DICK", "", function(str)
			SendLink(str)
		end)*/
		//StartPlaylist("Default Playlist", 1)

		local car = getcar(LocalPlayer():GetVehicle())

		if IsValid(car) and IsValid(car:getDoorOwner()) then
			if !IsValid(RashkinskMagnitolaFrame.PlaylistMenu) then
				RashkinskMagnitolaFrame.PlaylistMenu = OpenPlaylistEditMenu()
			end
		end
	end)


	for i = 0, 6-1 do
		CreateButton("radio" .. tostring(i + 1), 68 + i * 63.3, 142, 61, 20, radiomat, radiomatact, false, function()
			local car = getcar(LocalPlayer():GetVehicle())
			SendLink(MagnitolaConfig.Radios[i + 1].url)
			if IsValid(car) then
				car.CurRadio = MagnitolaConfig.Radios[i + 1].name
				car.RadioPreset = i + 1
			end
		end,
		function(self, w, h)
			draw.SimpleText(i + 1, "MagnitolaRadioButton", w - 20, h / 2, Color(208,153,155), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		end)
	end
	RashkinskMagnitolaFrame.CreatedButtons = Buttons
end