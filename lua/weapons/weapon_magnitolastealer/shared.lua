AddCSLuaFile()

if CLIENT then
    SWEP.PrintName = "Извлекатель магнитолы"
    SWEP.Slot = 5
    SWEP.SlotPos = 1
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
end

-- Variables that are used on both client and server

SWEP.Author = "AirBlack"
SWEP.Instructions = "ЛКМ или ПКМ - стащить магнитолу"
SWEP.Contact = ""
SWEP.Purpose = ""
//SWEP.IsDarkRPLockpick = true

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.ViewModel = Model("models/weapons/c_crowbar.mdl")
SWEP.WorldModel = Model("models/weapons/w_crowbar.mdl")

SWEP.UseHands = true

SWEP.Spawnable = true
SWEP.AdminOnly = true
SWEP.Category = "DarkRP (Utility)"

SWEP.Sound = Sound("physics/wood/wood_box_impact_hard3.wav")

SWEP.Primary.ClipSize = -1      -- Size of a clip
SWEP.Primary.DefaultClip = 0        -- Default number of bullets in a clip
SWEP.Primary.Automatic = false      -- Automatic/Semi Auto
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1        -- Size of a clip
SWEP.Secondary.DefaultClip = -1     -- Default number of bullets in a clip
SWEP.Secondary.Automatic = false        -- Automatic/Semi Auto
SWEP.Secondary.Ammo = ""

--[[-------------------------------------------------------
Name: SWEP:Initialize()
Desc: Called when the weapon is first loaded
---------------------------------------------------------]]
function SWEP:Initialize()
    self:SetHoldType("normal")
end

function SWEP:SetupDataTables()
    self:NetworkVar("Bool", 0, "IsLockpicking")
    self:NetworkVar("Float", 0, "LockpickStartTime")
    self:NetworkVar("Float", 1, "LockpickEndTime")
    self:NetworkVar("Float", 2, "NextSoundTime")
    self:NetworkVar("Int", 0, "TotalLockpicks")
    self:NetworkVar("Entity", 0, "LockpickEnt")
end

--[[-------------------------------------------------------
Name: SWEP:PrimaryAttack()
Desc: +attack1 has been pressed
---------------------------------------------------------]]

local function canLockpick(veh, ply, trace)
	if IsValid(veh) and veh:IsVehicle() and veh.VehicleTable and veh.VehicleTable.KeyValues and veh.VehicleTable.KeyValues.vehiclescript and veh.VehicleTable.KeyValues.vehiclescript:lower() != "scripts/vehicles/prisoner_pod.txt" 
    and IsValid(veh:getDoorOwner())
    and veh:getDoorOwner():GetNWBool("HasMagnitola", false)
    and trace.HitPos:DistToSqr(ply:GetShootPos()) < 10000 then
		return true
		
	end
	return false
end
function SWEP:PrimaryAttack()
    self:SetNextPrimaryFire(CurTime() + 2)
    if self:GetIsLockpicking() then return end

    self:GetOwner():LagCompensation(true)
    local trace = self:GetOwner():GetEyeTrace()
    self:GetOwner():LagCompensation(false)
    local ent = trace.Entity

    if not IsValid(ent) then return end
    local canLockpick = canLockpick(ent, self:GetOwner(), trace)//hook.Call("canLockpick", nil, self:GetOwner(), ent, trace)

    if canLockpick == false then return end
    /*if canLockpick ~= true and (
            trace.HitPos:DistToSqr(self:GetOwner():GetShootPos()) > 10000 or
            (not GAMEMODE.Config.canforcedooropen and ent:getKeysNonOwnable()) or
            (not ent:isDoor() and not ent:IsVehicle() and not string.find(string.lower(ent:GetClass()), "vehicle") and (not GAMEMODE.Config.lockpickfading or not ent.isFadingDoor))
        ) then
        return
    end*/

    self:SetHoldType("pistol")

    self:SetIsLockpicking(true)
    self:SetLockpickEnt(ent)
    self:SetLockpickStartTime(CurTime())
    local endDelta = hook.Call("lockpickTime", nil, self:GetOwner(), ent) or util.SharedRandom("DarkRP_Lockpick" .. self:EntIndex() .. "_" .. self:GetTotalLockpicks(), 10, 30)
    self:SetLockpickEndTime(CurTime() + endDelta)
    self:SetTotalLockpicks(self:GetTotalLockpicks() + 1)


    if IsFirstTimePredicted() then
        //hook.Call("lockpickStarted", nil, self:GetOwner(), ent, trace)
    end

    if CLIENT then
        self.Dots = ""
        self.NextDotsTime = SysTime() + 0.5
        return
    end

    local onFail = function(ply) if ply == self:GetOwner() then self:Fail() end end
	
	local ply = self:GetOwner()
	//local lockpicktime = math.random(0, endDelta+10)
	//print(lockpicktime)
	local veh = ent
	if IsValid(veh) and veh:IsVehicle() and veh.VehicleTable and veh.VehicleTable.KeyValues and veh.VehicleTable.KeyValues.vehiclescript and veh.VehicleTable.KeyValues.vehiclescript:lower() != "scripts/vehicles/prisoner_pod.txt" and veh:getDoorOwner() then
		if veh:getDoorOwner():GetNWBool("HasSignalization", false) then
			local shouldsignal = math.random(1,100)
			if shouldsignal <= 95 then
				veh:SetNWBool("WorkingRashSignalization", true)
				DarkRP.notify(veh:getDoorOwner(), 2, 4, "[АВТОЛОКАТОР] На вашем авто сработала сигнализация!")
			else
				DarkRP.notify(ply, 2, 4, "Вы сломали сигнализацию на авто. Она не сработает.")
				local owner = veh:getDoorOwner()
				owner:SetNWBool("HasSignalization", false)
				owner:SetPData("HasSignalization", false)
				timer.Simple(60, function()
					if IsValid(owner) and owner:IsPlayer() then
						DarkRP.notify(owner, 2, 6, "[АВТОЛОКАТОР] Сигнализация на вашем авто вышла из строя.")
					end
				end)
			end
		end
	end

    -- Lockpick fails when dying or disconnecting
    hook.Add("PlayerDeath", self, fc{onFail, fn.Flip(fn.Const)})
    hook.Add("PlayerDisconnected", self, fc{onFail, fn.Flip(fn.Const)})
    -- Remove hooks when finished
    //hook.Add("onLockpickCompleted", self, fc{fp{hook.Remove, "PlayerDisconnected", self}, fp{hook.Remove, "PlayerDeath", self}})
end

function SWEP:Holster()
    self:SetIsLockpicking(false)
    self:SetLockpickEnt(nil)
    return true
end

function SWEP:Succeed()
    self:SetHoldType("normal")

    local ent = self:GetLockpickEnt()
    self:SetIsLockpicking(false)
    self:SetLockpickEnt(nil)

    if not IsValid(ent) or !ent:IsVehicle() then return end

    //local override = hook.Call("onLockpickCompleted", nil, self:GetOwner(), true, ent)

    //if override then return end
	
	if IsValid(ent) and ent:IsVehicle() and ent.VehicleTable and ent.VehicleTable.KeyValues and ent.VehicleTable.KeyValues.vehiclescript and ent.VehicleTable.KeyValues.vehiclescript:lower() != "scripts/vehicles/prisoner_pod.txt" and IsValid(ent:getDoorOwner()) 
    and ent:getDoorOwner():GetNWBool("HasMagnitola", false) then
        local anotherply = ent:getDoorOwner()
        anotherply:SetPData("HasMagnitola", false)
		anotherply:SetNWBool("HasMagnitola", false)
		local ent = ents.Create("entity_magnitola")
		
		//local ang = self.Owner:EyeAngles()
		local tr = util.TraceEntity({
			start = self.Owner:GetShootPos(),
			endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 100,
			filter = self.Owner
		}, ent)
		//if tr.Hit then
			ent:SetPos(tr.HitPos)
			local ang = Angle(0,0,0)
			if tr.Hit and tr.HitNormal then
				ang = tr.HitNormal:Angle()
				ang:RotateAroundAxis(ang:Right(), -90)
			end
			ent:SetAngles(ang)
			ent:Spawn()
			ent:Activate()
			ent:SetIsStolen(true)
			ent:SetStealerPly(self:GetOwner())
			//ent:Setowning_ent(self.Owner)
			//ent:CPPISetOwner(self.Owner)
		//end
		//local tr = self:GetOwner():GetEyeTrace()
	end
end

function SWEP:Fail()
    self:SetIsLockpicking(false)
    self:SetHoldType("normal")

    //hook.Call("onLockpickCompleted", nil, self:GetOwner(), false, self:GetLockpickEnt())
    self:SetLockpickEnt(nil)
	hook.Remove("PlayerDisconnected", self)
	hook.Remove("PlayerDeath", self)
end

local dots = {
    [0] = ".",
    [1] = "..",
    [2] = "...",
    [3] = ""
}
function SWEP:Think()
    if not self:GetIsLockpicking() or self:GetLockpickEndTime() == 0 then return end

    if CurTime() >= self:GetNextSoundTime() then
        self:SetNextSoundTime(CurTime() + 1)
        local snd = {1,3,4}
        self:EmitSound("weapons/357/357_reload" .. tostring(snd[math.Round(util.SharedRandom("DarkRP_LockpickSnd" .. CurTime(), 1, #snd))]) .. ".wav", 50, 100)
    end
    if CLIENT and (not self.NextDotsTime or SysTime() >= self.NextDotsTime) then
        self.NextDotsTime = SysTime() + 0.5
        self.Dots = self.Dots or ""
        local len = string.len(self.Dots)

        self.Dots = dots[len]
    end

    local trace = self:GetOwner():GetEyeTrace()
    if not IsValid(trace.Entity) or trace.Entity ~= self:GetLockpickEnt() or trace.HitPos:DistToSqr(self:GetOwner():GetShootPos()) > 10000 then
        self:Fail()
    elseif self:GetLockpickEndTime() <= CurTime() then
        self:Succeed()
    end
end

function SWEP:DrawHUD()
    if not self:GetIsLockpicking() or self:GetLockpickEndTime() == 0 then return end

    self.Dots = self.Dots or ""
    local w = ScrW()
    local h = ScrH()
    local x, y, width, height = w / 2 - w / 10, h / 2 - 60, w / 5, h / 15
    //draw.RoundedBox(8, x, y, width, height, Color(10,10,10,120))
	surface.SetDrawColor(Color(0,0,0,200))
	surface.DrawRect(x,y,width,height)

    local time = self:GetLockpickEndTime() - self:GetLockpickStartTime()
    local curtime = CurTime() - self:GetLockpickStartTime()
    local status = math.Clamp(curtime / time, 0, 1)
    local BarWidth = status * (width - 16)
    local cornerRadius = math.Min(8, BarWidth / 3 * 2 - BarWidth / 3 * 2 % 2)
    //draw.RoundedBox(cornerRadius, x + 8, y + 8, BarWidth, height - 16, Color(255 - (status * 255), 0 + (status * 255), 0, 255))
	surface.SetDrawColor(Color(28,28,28))
	surface.DrawRect(x+8, y+8, BarWidth, height-16)

    draw.SimpleText("ХААА ВАРУЕМ МАГНИТОЛАС" .. self.Dots, "Trebuchet24", w / 2, y + height / 2, Color(255, 255, 255, 255), 1, 1)
end

function SWEP:SecondaryAttack()
    self:PrimaryAttack()
end